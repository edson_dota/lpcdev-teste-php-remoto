@extends('layouts.master')

@section('title', 'Clientes OMIE')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1>Clients OMIE</h1>
    </div>
</div>

<div class="row" style="margin-top: 20px">
    <div class="col-sm-12">
        <div class="alert alert-info" role="alert">
            Exibindo {{ $clientes->registros }} de {{ $clientes->total_de_registros }} registros. Página {{ $clientes->pagina }} de {{ $clientes->total_de_paginas }}
        </div>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Código Cliente OMIE</th>
                    <th>Nome Fantasia</th>
                    <th>E-mail</th>
                </tr>
            </thead>
            <tbody>

                @forelse($clientes->clientes_cadastro as $cliente)
                    <tr>
                        <td>{{ $cliente->codigo_cliente_omie }}</td>
                        <td>{{ $cliente->nome_fantasia }}</td>
                        <td>{{ $cliente->email }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">Nenhum cliente adicionado</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <nav aria-label="Page navigation">
          <ul class="pagination">
            <li>
                @for ($i =0; $i < $clientes->total_de_paginas; $i++)
                    @if ($clientes->pagina == $i+1)
                        <li class="active"><a href="#">{{ $i+1 }}</a></li>
                    @else
                        <li>{{ Html::linkRoute('omie.clients', $i+1, ['page' => $i+1]) }}</li>
                    @endif
                @endfor
            </li>
          </ul>
        </nav>
    </div>
</div>
@stop
