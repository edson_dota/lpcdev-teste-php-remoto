@extends('layouts.master')

@section('title', 'Adicionar novo cliente')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1>Adicionar novo cliente</h1>
    </div>
</div>

@isset($msg)
<div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p>{{ $msg }}</p>
</div>
@endisset


<div class="row" style="margin-top: 20px">
    <div class="col-sm-6">
        {{ Form::open(array('route' => 'client.save')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('name', 'Nome') }}
                {{ Form::text('name', null, [
                    'placeholder' => 'Digite o nome do cliente',
                    'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail:') }}
                {{ Form::email('email', null, [
                    'placeholder' => 'Digite o e-mail do cliente',
                    'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('telefone', 'Telefone') }}
                {{ Form::text('telefone', null, [
                    'placeholder' => 'Digite o telefone do cliente',
                    'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::submit('Salvar', ['class' => 'btn btn-success']) }}
            </div>

        {{ Form::close() }}
    </div>
</div>


@stop
