@extends('layouts.master')

@section('title', 'Listagem de clientes')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h1>Listagem de clientes</h1>
    </div>
</div>

<div class="row" style="margin-top: 20px">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th width="200px">Ações</th>
                </tr>
            </thead>
            <tbody>
                @forelse($clients as $client)
                    <tr>
                        <td>{{ $client->getId() }}</td>
                        <td>{{ $client->getName() }}</td>
                        <td>{{ $client->getNumeroTelefone() }}</td>
                        <td>{{ $client->getEmail() }}</td>
                        <td>
                            {{ Form::open(array('route' => 'client.remove')) }}
                                {{ form::hidden('id', $client->getId()) }}
                                {{ Form::submit('Apagar', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">Nenhum cliente adicionado</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>






@stop
