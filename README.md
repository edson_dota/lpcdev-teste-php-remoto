# Teste Programador Pleno/Senior LPCDev (Remoto) #

Refactory Sistema para cadastro de Clientes 

### Considerações ###
Com a entrada de um novo diretor na empresa LPCDev foi decidido criar um sistema interno para cadastro de clientes, integrando com o ERP OMIE através de sua API. No momento da requisição, a LPCDev só possuia um desenvolvedor Júnior, e devido a sua inexperiência, alguns padrões de código não foram utilizados e a integração com a OMIE ficou para um segundo momento.
Com a ajuda do Diretor, o desenvolvedor subiu um ambiente em Docker com Apache e MySQL para ser possível testar o sistema. Vendo a demanda aumentar por novas e mais complexas features, a LPCDev resolveu contratar um Desenvolvedor Sênior. O novo contratado deverá resolver as seguintes requisições:

### Requisito 1 ###

* Adicionar 2 campos na tabela "clientes" chamados numero_telefone e email 
* Adicionar os dois campos no form de cadastro (View add)
* Criar uma lista de clientes baseado na tabela clients do Mysql
* Remover cliente
* Fazer persistência utilizando Doctrine

### Requisito 2 (Integração OMIE) ###

* Como primeira integração com o ERP OMIE fazer uma listagem simples de clientes exibindo em uma tabela com **codigo_cliente_omie**, **nome_fantasia** e **email**

Informações para acesso a API:
documentação: http://developer.omie.com.br/service-list/

**modelo de requisição:**
```
#!json

POST /api/v1/geral/clientes/ HTTP/1.1
Host: app.omie.com.br
Content-type: application/json
Content-Length: 173

{"call":"ListarClientes","app_key":"1560731700","app_secret":"226dcf372489bb45ceede61bfd98f0f1","param":[{"pagina":1,"registros_por_pagina":100,"apenas_importado_api":"N"}]}
```



### Requisito 3 (Code Review) ###
* Fazer o code_review e o que poderia melhorar na forma que foi desenvolvido

### Instalando o Sistema ###
1. docker-compose up -d
1. docker exec -it webavapng bash
1. composer install
1. cp .env.example .env
1. php artisan migrate

Obs.: Como foi utilizado o Doctrine utilizar comandos: php artisan doctrine:schema:create e php artisan doctrine:schema:update

### Acessando o sistema ###
1. url: http://localhost:9999

### Banco de dados ###
* **user:** root
* **passwd:** abc123
* **host:** localhost:33668
* **database:** lpcdevdb


OBS: O sistema esta feito em Laravel e funcionando mas se quiser refazer com outro tipo de framework, fique a vontade. Pode-se usar bibliotecas externas. Pensar na migração do banco.

OBS2: Ao finalizar, disponibilizar o código no GitHub ou Bitbucket para análise (enviar email para luizpcam@lpcdev.com.br avisando do término)
