<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="clients")
 */
class Client {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $numero_telefone;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    public function __construct($name, $numero_telefone, $email) {
        $this->name  = $name;
        $this->numero_telefone = $numero_telefone;
        $this->email = $email;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getNumeroTelefone() {
        return $this->numero_telefone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setNumeroTelefone($numero_telefone) {
        $this->numero_telefone = $numero_telefone;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

}
