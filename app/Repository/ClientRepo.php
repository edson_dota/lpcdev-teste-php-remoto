<?php

namespace App\Repository;

use App\Entities\Client;
use Doctrine\ORM\EntityManager;


class ClientRepo {
    /**
     * @var string
     */
    private $class = 'App\Entities\Client';

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function create(Client $client) {
        $this->em->persist($client);
        $this->em->flush();
    }

    public function update(Client $client, $data) {
        $client->setName($data['name']);
        $client->setNumeroTelefone($data['numero_telefone']);
        $client->setEmail($data['email']);

        $this->em->persist($client);
        $this->em->flush();
    }

    public function getAllClients() {
        try {
            return $this->em->getRepository($this->class)->findAll();
        } catch(\Exception $e) {
            echo $e;exit;
        }
    }

    public function ClientOfId($id) {
        return $this->em->getRepository($this->class)->findOneBy([
            'id' => $id
        ]);
    }

    public function delete(Client $client) {
        $this->em->remove($client);
        $this->em->flush();
    }

    /**
     * create Client
     * @return Client
     */
    private function prepareData($data) {
        return new Client($data);
    }
}
