<?php

namespace App\Http\Controllers;

use App\Repository\ClientRepo as repo;
use App\Entities\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $repo;

    public function __construct(repo $repo) {
        $this->repo = $repo;
    }

    public function add() {
        return view('clients.add');
    }

    public function all() {
        $clients = $this->repo->getAllClients();
        return view('clients.list', ['clients' => $clients]);
    }

    public function remove(Request $request) {
        $client = $this->repo->ClientOfId($request->input('id'));
        $this->repo->delete($client);
        return redirect()->route('client.list');
    }

    public function save(Request $request) {
        $client = new Client($request->input('name'), $request->input('telefone'),
                             $request->input('email'));

        $this->repo->create($client);
        return view('clients.add', ['msg' => 'Cliente ' . $client->getName() . ' gravado com sucesso!']);
    }

}
