<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class OmieController extends Controller
{
    public function clients(Request $request) {
        $client = new Client();
        $pagina = 1;
        if(isset($request->page)) {
            $pagina = $request->page;
        }
        $registros_por_pagina = 100;
        $body_array = array(
            'call'       => 'ListarClientes',
            'app_key'    => '1560731700',
            'app_secret' => '226dcf372489bb45ceede61bfd98f0f1',
            'param' => array(array(
                    'pagina' => $pagina,
                    'registros_por_pagina' => 100,
                    'apenas_importado_api' => 'N'
                )
            )
        );
        $body = json_encode($body_array);

        $result = $client->request('POST', 'http://app.omie.com.br/api/v1/geral/clientes/', [
            'headers' => [
                'Content-type' => 'application/json'
            ],
            'json' => $body_array
        ]);

        $clientes = json_decode($result->getBody());
        return view('omie.clients', ['clientes' => $clientes]);
    }
}
