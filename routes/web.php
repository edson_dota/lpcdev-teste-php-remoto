<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('client/', 'ClientController@all')->name('client.list');
Route::get('client/add', 'ClientController@add')->name('client.add');
Route::post('client/save', 'ClientController@save')->name('client.save');
Route::post('client/remove', 'ClientController@remove')->name('client.remove');
Route::get('omie/clients', 'OmieController@clients')->name('omie.clients');
